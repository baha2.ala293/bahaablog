<?php

use Faker\Generator as Faker;

$factory->define(App\Blog::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'body'  => $faker->paragraph,
        'category_id' => function(){
            return factory(App\Category::class)->create()->id;
        }
    ];
});
