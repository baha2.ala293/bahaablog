<?php

use App\Blog;
use Faker\Generator as Faker;

$factory->define(App\Comment::class, function (Faker $faker) {
    return [
        'name'      => $faker->name,
        'email'     => $faker->safeEmail,
        'message'   => $faker->text,
        'blog_id'   => function(){
            return factory(App\Blog::class)->create()->id;
        }
    ];
});
