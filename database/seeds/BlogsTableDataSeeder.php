<?php

use App\Category;
use Illuminate\Database\Seeder;

class BlogsTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = Category::pluck('id')->toArray();
        foreach ($categories as $category) {
            factory('App\Blog',20)->create(['category_id' => $category]);
        }
    }
}
