@extends('frontend.layout')

@section('title','Bahaa Blog |'. $blog->title)

@section('content')

<section class="blog blog-single">

<div class="container">

    <div class="row">

        <div class="col-sm-8">

            <div class="blog-post-single">

                <a href="#" class="image">
                    <img src="{{ asset('public/frontend/assets/images/portfolio-img-large-1.png') }}" class="img-responsive img-rounded" />
                </a>

                <div class="post-details">

                    <h3>
                        <a href="{{ $blog->public_path() }}">{{ $blog->title }}</a>
                    </h3>
                    
                    <div class="post-meta">

                        <div class="meta-info">
                            <i class="entypo-calendar"></i> {{ $blog->created_at }}</div>

                            <div class="meta-info">
                                <i class="entypo-comment"></i>
                                {{ $blog->comments()->count() }} comments
                            </div>

                        </div>

                    </div>


                    <div class="post-content">

                        <p>{{ $blog->body }}</p>

                    </div>

                    <br />

                    <h3>
                        Comments
                         <small class="text-muted">{{ $blog->comments->count() }}</small>
                    </h3>

                    <hr />

                    <!-- Comments List -->
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if (session('status'))
                        <div class="alert alert-success">{{ session('status') }}</div>
                    @endif
                    <ul class="comments-list">

                        @forelse($blog->comments as $comment)
                        <!-- Comment -->
                        <li>
                            <div class="comment">
                                <div class="comment-thumb">
                                    <a href="#">
                                        <img src="{{ asset('public/frontend/assets/images/user-icon-1.png') }}" width="43" class="img-circle" />
                                    </a>
                                </div>
                                <div class="comment-content">
                                    <div class="comment-author">
                                        <a href="#">{{ $comment->name }}</a>
                                        <div class="comment-info">
                                            <span class="time">{{ $comment->created_at }}</span>
                                        </div>
                                    </div>
                                    <div class="comment-text">
                                        {{ $comment->message }}
                                    </div>
                                </div>
                            </div>
                        </li>
                        @empty 
                        @endforelse

                    </ul>



                    
                    <h3>
                        Leave a comment
                    </h3>
                    <hr />
                    
                    <form class="comment-form" method="post" action="{{ url('/comment')}}" >
                        @csrf
                        <input type="hidden" name="blog_id" value="{{ $blog->id }}">
                        <div class="form-group">
                            <input type="text" class="form-control" name="name" placeholder="Name:"  value="{{ old('name') }}" />
                        </div>

                        <div class="form-group">
                            <input type="text" class="form-control" name="email" placeholder="E-mail:"  value="{{ old('email') }}" />
                        </div>

                        <div class="form-group">
                            <textarea class="form-control" name="message" placeholder="Message:" rows="6" >{{ old('message') }}</textarea>
                        </div>

                        <button type="submit" class="btn btn-primary pull-right">
                            Comment
                        </button>

                    </form>

                </div>

            </div>


        </div>

    </div>

</section>

@endsection