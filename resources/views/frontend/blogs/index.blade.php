@extends('frontend.layout')

@section('title'.'Bahaa Blog | Blogs')

@section('content')

    @forelse($blogs as $blog)
        @include('frontend.blogs.blog-component', ['blog' => $blog])
    @empty 
        <h3>No Blogs Added Yet!</h3>
    @endforelse

    @if($blogs)
    <div class="text-center">
        <ul class="pagination">
            {{ $blogs->links() }}
        </ul>
    </div>
    @endif

@endsection