<div class="blog-post">
<div class="post-thumb">
    <a href="{{ $blog->public_path() }}">
        <img src="{{ asset('public/frontend/assets/images/blog-thumb-1.png') }}" class="img-rounded" />
        <span class="hover-zoom"></span>
    </a>
</div>
<div class="post-details">
    <h3>
        <a href="{{ $blog->public_path() }}">{{ $blog->title }}</a>
    </h3>
    <div class="post-meta">
        <div class="meta-info">
            <i class="entypo-calendar"></i> {{ $blog->created_at }}</div>
            <div class="meta-info">
                <i class="entypo-comment"></i>
                {{ $blog->comments()->count() }} comments
            </div>
        </div>
        <p>{{ str_limit($blog->body,150) }}</p>
    </div>
</div>