 <nav class="site-nav">
    <ul class="main-menu hidden-xs" id="main-menu">
        <li class="active">
            <a href="{{ url('/blogs') }}">
                <span>Blog</span>
            </a>
        </li>
    </ul>
    <div class="visible-xs">
        <a href="#" class="menu-trigger">
            <i class="entypo-menu"></i>
        </a>
    </div>
</nav>