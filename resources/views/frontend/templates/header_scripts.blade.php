<link rel="icon" href="{{ asset('public/frontend/assets/images/favicon.ico') }}">
<link rel="stylesheet" href="{{ asset('public/frontend/assets/css/bootstrap.css') }}">
<link rel="stylesheet" href="{{ asset('public/frontend/assets/css/font-icons/entypo/css/entypo.css') }}">
<link rel="stylesheet" href="{{ asset('public/frontend/assets/css/neon.css') }}">
<script src="{{ asset('public/frontend/assets/js/jquery-1.11.3.min.js') }}"></script>
<!--[if lt IE 9]><script src="{{ asset('public/frontend/assets/js/ie8-responsive-file-warning.js') }}"></script><![endif]-->
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->