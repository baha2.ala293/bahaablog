<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1" />

<title> @yield('title', 'BahaaBlog')</title>
@include('frontend.templates.header_scripts')


</head>
<body>

<div class="wrap">

<!-- Logo and Navigation -->
<div class="site-header-container container">

    <div class="row">

        <div class="col-md-12">

            <header class="site-header">

                <section class="site-logo">

                    <a href="{{ url('/') }}">
                        <img src="{{ asset('public/frontend/assets/images/logo@2x.png') }}" width="120" />
                    </a>

                </section>

                @include('frontend.templates.topnav')

            </header>

        </div>

    </div>

</div>  
<!-- Breadcrumb -->
<section class="breadcrumb">
    <div class="container">
        <div class="row">
            <div class="col-sm-9">
                <h1>BahaaBlog</h1>                
            </div>
        </div>
    </div>
</section>


<!-- Blog -->
<section class="blog">

    <div class="container">

        <div class="row">

            <div class="col-sm-8">

                <div class="blog-posts">
                    @yield('content')
                </div>

            </div>

            <div class="col-sm-4">
                <!-- List Sidebar -->
                <div class="sidebar">
                    <h3>
                        <i class="entypo-list"></i>
                        Categories
                    </h3>
                    <div class="sidebar-content">
                        <ul>
                            @forelse($categories as $category)
                            <li><a href="{{ url('/blogs') }}/category/{{ $category->id }}">{{ $category->name }}</a></li>
                            @empty 
                             <li>
                                <a href="#">No Categories yet!</span></a>
                            </li>
                            @endforelse
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>  


@include('frontend.templates.footer') 
</div>

<!-- Bottom scripts (common) -->
@include('frontend.templates.footer_scripts')

</body>
</html>