<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>BahaaBlog | Admin Login</title>

    @include('admin.templates.header_scripts')


</head>
<body class="page-body login-page login-form-fall" data-url="http://neon.dev">



    <div class="login-container">

        <div class="login-header login-caret">

            <div class="login-content">

                <a href="/" class="logo">
                    <img src="{{ asset('public/admin/assets/images/logo@2x.png') }}" width="120" alt="" />
                </a>

                <p class="description">Dear user, log in to access the admin area!</p>

            </div>

        </div>

        <div class="login-progressbar">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>

        <div class="login-form">

            <div class="login-content">

                <div class="form-login-error">
                </div>

                <form method="post" role="form"  action="{{ url('/admin/login') }}">
                    @csrf
                    <div class="form-group">

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="entypo-user"></i>
                            </div>
                            <input type="email" class="form-control" name="email"  placeholder="Email" autocomplete="off" />
                        </div>

                    </div>

                    <div class="form-group">

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="entypo-key"></i>
                            </div>

                            <input type="password" class="form-control" name="password" id="password" placeholder="Password" autocomplete="off" />
                        </div>

                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block btn-login">
                            <i class="entypo-login"></i>
                            Login
                        </button>
                    </div>


                </form>

            </div>

        </div>

    </div>


    <!-- Bottom scripts (common) -->
    @include('admin.templates.footer_scripts')

    <script src="{{ asset('public/admin/assets/js/jquery.validate.min.js') }}"></script>
    <script src="{{  asset('public/admin/assets/js/neon-login.js') }}"></script>


</body>
</html>