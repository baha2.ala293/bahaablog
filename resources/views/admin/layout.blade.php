<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <title>@yield('title', 'Bahaa Blog')</title>
    
    @include('admin.templates.header_scripts')


</head>
<body class="page-body" data-url="http://neon.dev">

<div class="page-container">
    
    <div class="sidebar-menu">

        <div class="sidebar-menu-inner">
            
            <header class="logo-env">

                <!-- logo -->
                <div class="logo">
                    <a href="/">
                        <img src="{{ asset('public/admin/assets/images/logo@2x.png') }}" width="120" alt="" />
                    </a>
                </div>

                <!-- logo collapse icon -->
                <div class="sidebar-collapse">
                    <a href="#" class="sidebar-collapse-icon"><!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
                        <i class="entypo-menu"></i>
                    </a>
                </div>

                                
                <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
                <div class="sidebar-mobile-menu visible-xs">
                    <a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
                        <i class="entypo-menu"></i>
                    </a>
                </div>

            </header>
            
                                    
            @include('admin.templates.sidenav')
            
        </div>

    </div>

    <div class="main-content">
        @yield('content')
        
        <!-- lets do some work here... -->
        <!-- Footer -->
        <footer class="main">
            &copy; 2015 <strong>Neon</strong> Admin Theme by <a href="http://laborator.co" target="_blank">Laborator</a>
        </footer>
    </div>

    
    
</div>




    @include('admin.templates.footer_scripts')

</body>
</html>