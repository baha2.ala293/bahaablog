<!-- Bottom scripts (common) -->
<script src="{{ asset('public/admin/assets/js/gsap/TweenMax.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/js/bootstrap.js') }}"></script>
<script src="{{ asset('public/admin/assets/js/joinable.js') }}"></script>
<script src="{{ asset('public/admin/assets/js/resizeable.js') }}"></script>
<script src="{{ asset('public/admin/assets/js/neon-api.js') }}"></script>


<!-- JavaScripts initializations and stuff -->
<script src="{{ asset('public/admin/assets/js/neon-custom.js') }}"></script>

<!-- Demo Settings -->
<script src="{{ asset('public/admin/assets/js/neon-demo.js') }}"></script>