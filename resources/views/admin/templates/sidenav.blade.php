<ul id="main-menu" class="main-menu">
    <li class="has-sub">
        <a href="{{ url('admin/categories') }}">
            <i class="entypo-layout"></i>
            <span class="title">Categories</span>
        </a>
    </li>
    <li class="has-sub">
        <a href="{{ url('/admin/blogs') }}">
            <i class="entypo-newspaper"></i>
            <span class="title">Blogs</span>
        </a>
    </li>
     <li class="has-sub">
        <form action="{{ url('/admin/logout') }}" method="post">
        @csrf
        
            <a href="#">
                <i class="entypo-logout"></i>
                <button type="submit" style="background: #303641;border: none;"><span>Logout</span></button>
            </a>
        </form>
    </li>
</ul>