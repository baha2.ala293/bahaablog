@extends('admin.layout')

@section('title','categories')

@section('content')
<h3>
    Categories
</h3>
<br />

<div class="row">
    <div class="col-md-12">

        <table class="table table-bordered responsive">
            <thead>
                <tr>
                    <th>Name</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($categories as $category)
                <tr>
                    <td>{{ $category->name }}</td>
                </tr>  
                @endforeach 
            </tbody>
        </table>

    </div>
</div>
@endsection