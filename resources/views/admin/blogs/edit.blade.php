@extends('admin.layout')

@section('title','Blogs | '. $blog->title)

@section('content')
<h3>
    Blogs | {{ $blog->title }}
</h3>
<br />

<div class="row">
    <div class="col-md-12">
      @include('admin.templates.alerts')

        <div class="panel panel-primary" data-collapsed="0">

            <div class="panel-body">


                <form role="form" class="form-horizontal form-groups-bordered" method="post" 
                        action="{{ url('admin/blogs') }}/{{ $blog->id }}">
                    @csrf
                    {{ method_field('PATCH') }}
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Category</label>

                        <div class="col-sm-5">
                            <select class="form-control" name="category_id">
                                @foreach ($categories as $category)
                                    <option value="{{ $category->id }}" 
                                        {{ $blog->category_id == $category->id ? "selected" : "" }}> 
                                        {{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Title</label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control" id="field-1" name="title" value="{{ $blog->title }}">
                        </div>
                    </div>



                    <div class="form-group">
                        <label for="field-ta" class="col-sm-3 control-label">Body</label>
                        <div class="col-sm-5">
                            <textarea class="form-control" id="field-ta" placeholder="Textarea" name="body">
                                {{ $blog->body }}
                            </textarea>
                        </div>
                    </div>



                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-5">
                            <button type="submit" class="btn btn-default">Save</button>
                            <a href="{{ url('/admin/blogs') }}">Cancel</a>
                        </div>
                    </div>
                </form>

            </div>

        </div>

    </div>
</div>
@endsection('content')