@extends('admin.layout')

@section('title','Blogs')

@section('content')
<h3>
    Blogs
</h3>

<br />

<div>
    <a href="{{ url('admin/blogs') }}/create" class="btn btn-success">Create</a>
</div>
<br>
@include('admin.templates.alerts')

<div class="row">
    <div class="col-md-12">

        <table class="table table-bordered responsive">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Body</th>
                    <th>Published</th>
                    <th>Created at</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($blogs as $blog)
                <tr>
                    <td>{{ $blog->title }}</td>
                    <td>{{ str_limit($blog->body) }}</td>
                    <td>{{ $blog->is_published }}</td>
                    <td>{{ $blog->created_at }}</td>
                    <td>    
                        <div class="btn-group">
                            <button type="button" class="btn btn-blue dropdown-toggle" data-toggle="dropdown">
                                Action <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu dropdown-blue" role="menu">
                                <li><a href="{{ $blog->admin_path() }}">view</a></li>
                                <li><a href="{{ $blog->admin_path() }}/edit">edit</a></li>
                                <li>
                                    <form action="{{ $blog->admin_path() }}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" style="background: #0072bc;border: none;color: white;">Delete</button>
                                    </form>
                                </li>
                            </ul>
                        </div>
                    </td>
                </tr>
                @empty
                <tr>
                    <td></td>
                    <td></td>
                    <td>No Blogs Yet</td>
                    <td></td>
                    <td> </td>
                </tr>  
                @endforelse
            </tbody>
        </table>

    </div>
</div>

@if($blogs)
<div class="text-center">
    {{ $blogs->links() }}
</div>
@endif
@endsection