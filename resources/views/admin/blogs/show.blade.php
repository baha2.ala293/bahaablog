@extends('admin.layout')

@section('title','Blogs | '. $blog->title)

@section('content')
<h3>
    Blogs | {{ $blog->title }}
</h3>
<br />

<div class="row">
    <div class="col-md-12">

        <div class="panel panel-primary" data-collapsed="0">

            <div class="panel-heading">
                <div class="panel-title"></div>
            </div>

            <div class="panel-body">

                <form role="form" class="form-horizontal form-groups-bordered">
                    <div class="form-group">
                        <label for="field-2" class="col-sm-3 control-label">Title</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control"  disabled value="{{ $blog->title }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-2" class="col-sm-3 control-label">Created at</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control"  disabled value="{{ $blog->created_at }}">
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="field-ta" class="col-sm-3 control-label">Body</label>

                        <div class="col-sm-5">
                            <textarea class="form-control" id="field-ta"  disabled">{{ $blog->body }}</textarea>
                        </div>
                    </div>

                  <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-5">
                            <a href="{{ url('/admin/blogs') }}">Cancel</a>
                        </div>
                    </div>
                </form>

            </div>

        </div>

    </div>
</div>
@endsection('content')