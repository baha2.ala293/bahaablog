** Bahaa Blog**

# Approach

I created Simple Blog system using laravel verion 5.7.

I focused completing the task by using a TDD approach that uses the phpunit with some feature tests and other unit test. I thought that by using a TDD approach, I can identify the sceanarios before making the actual code to perform the conversion. This ensures that before coding, I understand the requirements fully as a test case with pseudo code will be written beforehand.

Note: I didn't write all possible test cases for the app.

After the test cases were written, I started writing the validation code first.

Finally, after writing all functionality of the code and running it against the test cases and passing, I made a simple frontend using neon theme.

# Assumptions

PHP will be installed on the users computer.
[Composer Installed](https://getcomposer.org/download/)

## Installation
*  ### Server requirements
    - PHP >= 7.1.3
    - OpenSSL PHP Extension
    - PDO PHP Extension
    - Mbstring PHP Extension
    - Tokenizer PHP Extension
    - XML PHP Extension

* cd to your server directory and pull the project : https://gitlab.com/baha2.ala293/bahaablog.git
* cd to the project directory and run > composer update
* run > cp .example.env .env
* create an empty database and set the name of the database with credentials in the .env file
* run > chmod -R 777 storage
* run > php artisan key:generate
* run > php artisan config:cache 
* run > php artisan migrate
* run > php artisan db:seed


## Run tests
*please make sure sqlite is installed and running on computer or define antoher database configuration on phpunit.xml in root directory
 > vendor/bin/phpunit


## Admin path

*{base_url}/admin/login


## Admin credentials

* please make sure you seed the database with some dummy data and an admin user will be created with credentials below:
email: bahaa@blog.com 
password : password
