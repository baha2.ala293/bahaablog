<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BlogsTest extends TestCase
{
   
   use WithFaker, RefreshDatabase;

  /** @test */
   public function only_admin_can_create_blogs()
   {

      $data = factory('App\Blog')->raw();

      $this->post('/admin/blogs', $data)->assertRedirect(url('/admin/login'));
   }

   /** @test */
   public function an_admin_can_create_a_blog()
   {

      $this->withoutExceptionHandling();
      
      $this->actingAs(factory('App\User')->create());

      $data = factory('App\Blog')->raw();

      $this->post('/admin/blogs', $data)->assertRedirect('/admin/blogs');

      $this->assertDatabaseHas('blogs', $data);

      $this->get('/admin/blogs')->assertSee($data['title']);

   }

   /** @test */
   public function an_admin_can_delete_a_blog()
   {

      $this->withoutExceptionHandling();
      
      $this->actingAs(factory('App\User')->create());

      $blog = factory('App\Blog')->create();

      $this->delete($blog->admin_path())->assertRedirect('/admin/blogs');

   }

  /** @test */
   public function an_admin_can_view_a_blog()
   {
      $this->actingAs(factory('App\User')->create());

      $blog = factory('App\Blog')->create();

      $this->get($blog->admin_path())
                ->assertSee($blog->title)
                ->assertSee($blog->body);
   }

   /** @test */
   public function a_blog_requires_a_title()
   {
      $this->actingAs(factory('App\User')->create());

      $data = factory('App\Blog')->raw(['title' => '']);

      $this->post('/admin/blogs', $data )->assertSessionHasErrors('title');
     
   }

   /** @test */
   public function a_blog_requires_a_body()
   {
      $this->actingAs(factory('App\User')->create());

      $data = factory('App\Blog')->raw(['body' => '']);

      $this->post('/admin/blogs', $data)->assertSessionHasErrors('body');
   }

   /** @test */
   public function a_blog_requires_a_category_id()
   {
      $this->actingAs(factory('App\User')->create());

      $data = factory('App\Blog')->raw(['category_id' =>  null]);

      $this->post('/admin/blogs', $data)->assertSessionHasErrors('category_id');
   }

   /** @test */
   public function a_visitor_can_view_a_published_blog()
   {

      $blog = factory('App\Blog')->create();

      $this->get($blog->public_path())
                ->assertSee($blog->title)
                ->assertSee($blog->body);

   }

    /** @test */
   public function a_visitor_can_not_view_a_not_published_blog()
   {

      $blog = factory('App\Blog')->create(['is_published' => '0']);

      $this->get($blog->public_path())
                ->assertStatus(404);

   }

   /** @test */
   public function a_visitor_can_comment_a_published_blog()
   {
      $blog = factory('App\Blog')->create();

      $comment = factory('App\Comment')->create(['blog_id' => $blog->id]);

      $this->get($blog->public_path())
                        ->assertSee($comment->name)
                        ->assertSee($comment->message);

   }






}
