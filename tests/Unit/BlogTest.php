<?php

namespace Tests\Unit;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class BlogTest extends TestCase
{
    use RefreshDatabase;
    

    /** @test */
    public function it_has_an_admin_path()
    {
        $blog = factory('App\Blog')->create();

        $this->assertSame(url('/admin/blogs/')."/".$blog->id, $blog->admin_path());
    }

    /** @test */
    public function it_has_a_public_path()
    {
        $blog = factory('App\Blog')->create();

        $this->assertSame(url('/blogs/')."/".$blog->id, $blog->public_path());
        
    }

    /** @test */
    public function a_blog_has_comments()
    {
        $blog = factory('App\Blog')->create();

        $this->assertInstanceOf(Collection::class, $blog->comments);
    }
}
