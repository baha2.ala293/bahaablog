<?php

namespace Tests\Unit;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CategoryTest extends TestCase
{
    

    use RefreshDatabase;

    /** @test */
    public function a_category_has_projects()
    {
        $category = factory('App\Category')->create();

        $this->assertInstanceOf(Collection::class, $category->blogs);

    }
}
