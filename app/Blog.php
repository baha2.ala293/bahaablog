<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $guarded = [];


    public function admin_path()
    {
        return url('/admin/blogs/'.$this->id);
    }

    public function public_path()
    {
        return url('blogs/'.$this->id);
    }

    public function scopePublished($query)
    {
        return $query->where('is_published', '1');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function getCreatedAtAttribute($created_at)
    {
        return $this->attributes['created_at'] = Carbon::parse($created_at)->format('Y-M-d H:i');
    }

    public function getIsPublishedAttribute($is_published)
    {
        return $this->attributes['is_published'] = $is_published ? 'yes' : 'no';
    }

}
