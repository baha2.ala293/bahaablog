<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $guarded = [];

    public function getCreatedAtAttribute( $created_at )
    {
        return $this->attributes['created_at'] = Carbon::parse($created_at)->diffForHumans();
    }
}
