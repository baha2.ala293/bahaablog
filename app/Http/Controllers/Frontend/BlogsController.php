<?php

namespace App\Http\Controllers\Frontend;

use App\Blog;
use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BlogsController extends Controller
{
    
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $blogs = Blog::Published()->paginate(10);

        return view('frontend.blogs.index', compact('blogs'));
    }


    public function blogsByCategory(Category $category)
    {
        $blogs = $category->blogs()->paginate(10);

        return view('frontend.blogs.index', compact('blogs'));

    }


    public function show($id)
    {
        $blog = Blog::Published()->findOrFail($id);
        return view('frontend.blogs.show', compact('blog'));
    }
}
