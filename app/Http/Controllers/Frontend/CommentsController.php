<?php

namespace App\Http\Controllers\Frontend;

use App\Comment;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    



    public function store()
    {
        $data = request()->validate([
            'name'      => 'required|string',
            'email'     => 'required|email',
            'message'   => 'required|string',
            'blog_id'   => 'required|exists:blogs,id'
        ]);

        Comment::create($data);

        return redirect()->back()->with('status', 'Thanks for your comment!');

    }
}
