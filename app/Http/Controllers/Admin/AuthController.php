<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    



    public function index()
    {
        return view('admin.auth.login');
    }


    public function login()
    {
        $data = request()->validate([
            'email'     =>  'required|email',
            'password'  =>  'required'
        ]);

        return auth()->attempt($data) ? redirect(url('/admin/blogs')) : redirect()->back();
    }

    public function logout()
    {
        auth()->logout();
        return redirect(url('/admin/login'));
    }
}
