<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    


    public function index()
    {
        $categories = Category::all();

        return view('admin.categories.index', compact('categories'));
    }



    public function store()
    {
        $data = request()->validate(['name' => 'required|string']);

        Category::create($data);

        return redirect('/admin/categories');
    }

}
