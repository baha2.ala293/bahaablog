<?php

namespace App\Http\Controllers\Admin;

use App\Blog;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BlogsController extends Controller
{
    
    public function index()
    {
        $blogs = Blog::paginate();

        return view('admin.blogs.index', compact('blogs'));
    }


    public function show(Blog $blog)
    {
        return view('admin.blogs.show', compact('blog'));
    }


    public function create()
    {
        return view('admin.blogs.create');
    }


    public function store()
    { 
        $data = request()->validate([
            'category_id'   =>  'required',
            'title'         =>  'required|string',
            'body'          =>  'required|string'
         ]);

        Blog::create($data);
        
        return redirect('/admin/blogs')->with('status','Created!');
    }


    public function edit(Blog $blog)
    {
        return view('admin.blogs.edit', compact('blog'));
    }

    public function update(Request $request,$id)
    { 
        $data = request()->validate([
            'category_id'   =>  'required',
            'title'         =>  'required|string',
            'body'          =>  'required|string'
         ]);

        Blog::find($id)->update($data);
        
        return redirect('/admin/blogs')->with('status','Updated!');
    }

    public function destroy(Blog $blog)
    {
        $blog->delete();
        return redirect('admin/blogs')->with('status', 'Deleted!');
    }
}
