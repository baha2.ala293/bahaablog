<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/blogs');
});

/**
 * Admin Routes
 */
Route::group(['prefix' => '/admin', 'namespace' => 'Admin' ], function() {

    Route::get('login'             , 'AuthController@index')->name('login');
    Route::post('login'            , 'AuthController@login');
    Route::post('logout'           , 'AuthController@logout')->middleware('auth');

    Route::resource('categories'    , 'CategoriesController')->middleware('auth');
    Route::resource('blogs'         , 'BlogsController')->middleware('auth');

});

/**
 * Frontend Routes
 */
Route::group(['prefix' => '/', 'namespace' => 'Frontend' ], function() {

    Route::get('/blogs'                      , 'BlogsController@index');
    Route::get('/blogs/{blog}'               , 'BlogsController@show');
    Route::get('/blogs/category/{category}'  , 'BlogsController@blogsByCategory');

    Route::post('/comment'                   , 'CommentsController@store');

});

